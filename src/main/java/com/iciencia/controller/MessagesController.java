package com.iciencia.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.iciencia.dto.MessageDto;
import com.iciencia.service.MessageService;

@RestController
@RequestMapping("message")
public class MessagesController {

	@Autowired
	private MessageService messageService;

	@GetMapping
	public ResponseEntity<MessageDto> generateMessageWs() {
		return new ResponseEntity<>(messageService.generateMesage(), HttpStatus.OK);
	}
}
